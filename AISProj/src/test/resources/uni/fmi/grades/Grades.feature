Feature: Grading
	
	Scenario Outline: Adding a grade
    Given user opens grading screen
    When they enter a students id: "<id>"
    And enters a students grade: "<grade>"
    And presses confirm button
    Then the message: "<message>" is shown

    Examples: 
      | id    | grade | message	| 
      | 1 		|     5 | Grade successfully added 											|
      | 1     |     7 | A grade must only be a number between 2 and 6 |
			| 1			|			a | Please use only numbers as grades or ids			|
			| a			|			3 | Please use only numbers as grades or ids			|
			| 1			|		    | Please input a grade							 						|
			| 100		|		  4 | There is no student with this id	 						|
			| 			|		  3 | Please input an id								 						|