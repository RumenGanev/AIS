Feature: Log into the system 

	Background: 
		Given Login screen shows

  Scenario: Login with valid user data
    When username "Mark" is inputed
    And password "1234" is inputed
    And the login button is pressed
    Then message: "Successfull login".

   Scenario: Login without a username
    When password "1234" is inputed
    And the login button is pressed
    Then message: "Unput username and password".
    
   Scenario: Login without a password
    When username "Mark" is inputed
    And the login button is pressed
    Then message: "Unput username and password".
    
   Scenario: Login without a any information
    When the login button is pressed
    Then message: "Unput username and password".
    
   Scenario: Login with invalid username
    When username "Marcus" is inputed
    And password "1234" is inputed
    And the login button is pressed
    Then message: "Wrong username or password".
    
   Scenario: Login with invalid password
    When username "Mark" is inputed
    And password "123" is inputed
    And the login button is pressed
    Then message: "Wrong username or password".
    
   Scenario: Login with invalid username and password
    When username "Marcus" is inputed
    And password "1234" is inputed
    And the login button is pressed
    Then message: "Wrong username or password".
    
    