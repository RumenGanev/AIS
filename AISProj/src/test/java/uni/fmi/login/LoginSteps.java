package uni.fmi.login;

import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uni.fmi.login.model.LoginScreenModel;

public class LoginSteps {

	private LoginScreenModel loginModel;

	@Given("^Login screen shows$")
	public void openLoginScreen() throws Throwable {
	    loginModel = new LoginScreenModel();
	}

	@When("^username \\\"([^\\\"]*)\\\" is inputed$")
	public void inputUsername(String username) throws Throwable {
	    loginModel.setUsername(username);
	}

	@When("^password \\\"([^\\\"]*)\\\" is inputed$")
	public void inputPassword(String password) throws Throwable {
	    loginModel.setPassword(password);
	}

	@When("^the login button is pressed$")
	public void clickLoginButton() throws Throwable {
	    loginModel.clickLoginButton();
	}

	@Then("^message: \"([^\"]*)\".$")
	public void successMessage(String message) throws Throwable {
	    assertEquals(message, loginModel.getMessage());
	}
}
