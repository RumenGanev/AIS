package uni.fmi.grades;

import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import uni.fmi.grades.model.GradeScreenModel;

public class GradingSteps {
	
	private GradeScreenModel model;
	
	@Given("^user opens grading screen$")
	public void openGradingScreen() throws Throwable {
	    model = new GradeScreenModel();
	}

	@When("^they enter a students id: \"([^\"]*)\"$")
	public void enterStudentId(String id) throws Throwable {
	    model.setStudentId(id);
	}

	@When("^enters a students grade: \"([^\"]*)\"$")
	public void enterStudentGrade(String grade) throws Throwable {
	    model.addGrade(grade);
	}

	@When("^presses confirm button$")
	public void pressConfrim() throws Throwable {
	    model.clickConfrirm();
	}

	@Then("^the message: \"([^\"]*)\" is shown")
	public void showMessage(String message) throws Throwable {
	   assertEquals(message, model.getMessage());
	}
}
