package uni.fmi.grades.model;

import uni.fmi.grading.service.GradingService;

public class GradeScreenModel {

	private String studentId;
	private int convertedId;
	private String grade;
	private int convertedGrade;
	private String message;

	public void setStudentId(String id) {
		this.studentId = id;
	}

	public void addGrade(String grade) {
		this.grade = grade;
	}
	
	public void clickConfrirm() {
		
		if(convertId() == true && convertGrade() == true) {

			boolean doesExist = GradingService.doesStudentExist(convertedId);
			
			if(!doesExist) {
				message = "There is no student with this id";
				return;
			}
			
			boolean gradeAdded = GradingService.addGrade(convertedId, convertedGrade);
			message = "Grade successfully added";
		}
	}
	
	public String getMessage() {
		return message;
	}
	
	private boolean convertId() {
		try {
			if (studentId == "") {
				message = "Please input an id";
				return false;
			}
			
			int studentIdInt = Integer.parseInt(studentId);
			convertedId = studentIdInt;
			
		} catch (Exception e) {
			message = "Please use only numbers as grades or ids";
			return false;
		}
		
		return true;
	}
	
	private boolean convertGrade() {
		try {
			if (grade == "") {
				message = "Please input a grade";
				return false;
			}
			
			int convGrade = Integer.parseInt(grade);
			convertedGrade = convGrade;
			
			if(convGrade < 2 || convGrade > 6) {
				message = "A grade must only be a number between 2 and 6";
				return false;
			}
			
		} catch (Exception e) {
			message = "Please use only numbers as grades or ids";
			return false;
		}
		
		return true;
	}
}
