package uni.fmi.grading.service;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import uni.fmi.models.Student;

public class GradingService {
	private static List<Student> students = Collections.singletonList(new Student());
	
	public static boolean doesStudentExist(int id) {
		boolean exists = false;
		for (Student student : students) {
			if(id == student.getId()) {
				exists = true;
			}
		}
		return exists;
	}
	
	public static boolean addGrade(int id, int grade) {
		for (Student student : students) {
			if(id == student.getId()) {
				student.addGrade(grade);
				return true;
			}
		}
		return false;
	}
}
