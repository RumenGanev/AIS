package uni.fmi.models;
import java.util.*;

public class Parent extends User {

	private String username;
    private String password;
	private Set<Student> children;
    
    public Parent() {
    }
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ArrayList<Integer> checkGrades() {
    	ArrayList<Integer> finalGrades = new ArrayList<Integer>();
    	for (Student student : children) {
    		Integer[] grades = new Integer[student.getGrades().toArray().length];
    		Integer[] studentArray = student.getGrades().toArray(grades);
    		for (int i = 0; i < studentArray.length; i++) {
    			finalGrades.add(studentArray[i]);
			}
		}
        return finalGrades;
    }
    
    public Set<Student> getChildren() {
        return children;
    }

    public void setChildren(Set<Student> children) {
        this.children = children;
    }

}