package uni.fmi.models;
import java.util.*;

public class SchoolClass {
	
    private Teacher teacher;
    private Set<String> subjects;
    private Set<Student> students;

    public SchoolClass() {
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subj) {
        this.subjects = subj;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }
    
    public void addStudent(Student student) {
    	this.students.add(student);
    }
    
    public Teacher getTeacher() {
    	return teacher;
    }
    
    public void setTeacher(Teacher teacher) {
    	this.teacher = teacher;
    }
}