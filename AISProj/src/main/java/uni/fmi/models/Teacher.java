package uni.fmi.models;
import java.util.*;


public class Teacher extends User {
	
	private String username;
    private String password;
    private SchoolClass schoolClass;
    private List<Student> students;

    public Teacher() {
    }

    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
        schoolClass.setTeacher(this);
    }
    
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}