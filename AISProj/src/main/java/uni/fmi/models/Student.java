package uni.fmi.models;
import java.util.*;

public class Student extends User {

    private SchoolClass schoolSlass;
    private ArrayList<Integer> grades;
    private String username;
    private String password;
    private Teacher teacher;
    private Set<Parent> parents;

    public Student() {
    	grades = new ArrayList<Integer>();
    }

    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SchoolClass getSchoolClass() {
        return schoolSlass;
    }

    public void setClass(SchoolClass schoolClass) {
        this.schoolSlass = schoolClass;
        schoolClass.addStudent(this);
    }

    public Set<Parent> getParents() {
    	return parents;
    }

    public void setParent(Set<Parent> parents) {
        this.parents = parents;
    }

    public ArrayList<Integer> getGrades() {
        return grades;
    }

    public void addGrade(int grade) {
        grades.add(grade);
    }

}