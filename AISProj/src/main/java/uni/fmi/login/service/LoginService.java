package uni.fmi.login.service;
import java.util.Collections;
import java.util.List;

import uni.fmi.models.User;

public class LoginService {
	
	private static List<User> users = Collections.singletonList(new User("Mark", "1234"));
	
	public static String login(String username, String password) {
		
		if(username == null || password == null) {
			return "Unput username and password";
		}
		
		boolean doesUserExist = users.stream().anyMatch(u->username.equals(u.getUsername()) && password.equals(u.getPassword()));
		return doesUserExist ? "Successfull login" : "Wrong username or password";
	}

}
